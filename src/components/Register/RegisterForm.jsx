import React, { useState } from 'react'
import axios from 'axios'

import './RegisterForm.css'

export default props => {
    // Definição dos estados que são usados para guardar o valor dos inputs 
    // do formulário e posteriormente enviá-los à API
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirmation, setPasswordConfirmation] = useState('')
    const [birthDate, setBirthDate] = useState('')
    const [gender, setGender] = useState('')
    
    // Função que é executada ao serem dectadas mudanças em qualquer input do formulário
    const handleInputChange = e => {
        const value = e.target.value

        // Verifica qual input foi modificado e altera o valor do estado equivalente
        switch (e.target.name) {
            case "name":
                setName(value)
                break
            case "email":
                setEmail(value)
                break
            case "password":
                setPassword(value)
                break
            case "passwordConfirmation":
                setPasswordConfirmation(value)
                break
            case "birthDate":
                setBirthDate(value)
                break
            case "gender":
                setGender(value)
                break
            default:
        }
    }
                    
    // Código que é executado ao enviar o formulário
    const handleSubmit = e => {
        e.preventDefault()
        
        // Escreve o que será o corpo da requisição na variável postBody,
        // de acordo com os estados de cada valor
        const postBody = {
            "user": {
                "name": name,
                "email": email,
                "gender": gender,
                "password": password,
                "password_confirmation": passwordConfirmation,
                "birthdate": birthDate
            }
        }

        // Realiza a requisição HTTP com o corpo definido anteriormente
        axios.post('https://wolfbook.herokuapp.com/sign_up', postBody)
            .then( response => {
                console.log(response)
            }).catch(error => {
                alert("Não foi possível realizer o cadastro")
            })
    }

    // Retorna o formulário a ser renderizado na tela
    return (
        <form className="register-form" onSubmit={handleSubmit}>
            <h3>{props.children}</h3>

            <input type="text" name="name"placeholder="Nome" onChange={handleInputChange}/>
            <input type="email" name="email" placeholder="Email" onChange={handleInputChange}/>
            <input type="password" name="password" placeholder="Nova senha" onChange={handleInputChange}/>
            <input type="password" name="passwordConfirmation" placeholder="Confirmar nova senha" onChange={handleInputChange}/>

            <label>Data de nascimento:</label>
            <input type="date" name="birthDate" placeholder="Data de" onChange={handleInputChange}/>

            <div>
                <label>Gênero:</label>
                <input type="radio" id="male" name="gender" value="F" onChange={handleInputChange}/>
                <label for="male">Feminino</label>
                <input type="radio" id="female" name="gender" value="M" onChange={handleInputChange}/>
                <label for="female">Masculino</label>
            </div>

            <input type="submit" value="Cadastrar"/>
        </form>
    )
}