import React, { useState, useContext } from 'react'
import { Context } from '../../../Context/AuthContext'

import "./LoginForm.css"

var postBody
export default props => {
    // Declara os estados que serão usados na requisição HTTP
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const {handleLogin} = useContext(Context)

    // Função executada ao dectatar mudança em algum dos inputs
    // que atualiza o valor dos estados de acordo com a entrada no input em questão
    const handleInputChange = e => {
        if (e.target.type === "email") {
            setEmail(e.target.value)
        } else if (e.target.type === "password") {
            setPassword(e.target.value)
        }
    }

    // Declara o corpo da requisição que será enviada de acordo
    // com os estados do componente
    postBody = {
        "user": {
            "email": email,
            "password": password
        } 
    }
    
    // Retorna o formulário que será renderizado na tela
    return (
        <div className="login-form">
            <input type="email" placeholder="Email" onChange={handleInputChange}/>
            <input type="password" placeholder="Senha" onChange={handleInputChange}/>
            
            <button className="login-button" onClick={handleLogin}>Logar</button>
        </div>
    )  
}

export { postBody }