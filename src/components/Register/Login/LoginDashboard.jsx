import React, { useState } from 'react'

import LoginModal from './LoginModal'

import './LoginDashboard.css'

export default props => {
    // Declara o estado que regula a exibição do modal de login 
    const [canShow, setCanShow] = useState(false)

    // Funções que mudam o estado canShow, dependendo se o modal 
    // de login pode ser exibido ou não
    const showLoginModal = () => setCanShow(true)
    const hideLoginModal = () => setCanShow(false)
    
    // Retorna o conteúdo que será renderizado na tela, 
    // próximo do formulário de cadastro
    return (
        <div className="login-dashboard">
            <h3>{props.children}</h3>

            <LoginModal canShow={canShow} handleClose={hideLoginModal}>
                <h1>Fazer login</h1>
            </LoginModal>

            <button type="button" onClick={showLoginModal}>
                {props.buttonText}
            </button>
        </div>
    )
}