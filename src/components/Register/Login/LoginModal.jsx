import React from 'react'

import './LoginModal.css'
import LoginForm from './LoginForm';

export default props => {
    // Variável que determina se o modal será exibido ou não
    const toggleClass = props.canShow ? "login-modal display-block" : "login-modal display-none";

    // Retorna o componente que será renderizado na tela como um modal
    return (
        <div className={toggleClass}>
            <section className="login-modal-main">
                <div>
                    {props.children}
                    <button onClick={props.handleClose}>X</button>
                </div>
                
                <LoginForm></LoginForm>
            </section>
        </div>
    )
}