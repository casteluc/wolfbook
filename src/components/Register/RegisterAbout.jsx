import React from 'react'

import './RegisterAbout.css'

export default props => {
    // Retorna o conteúdo a ser renderizado na tela
    return (
        <div className="register-about">
            <h1>{props.title}</h1>
            <h2>{props.children}</h2>
        </div>
    )
}