import React, { useContext } from 'react'
import { Context } from '../../Context/AuthContext'

import './LogoutButton.css'

export default props => {
    const { handleLogout } = useContext(Context)

    return (
        <button className="logout-button" onClick={handleLogout}>Logout</button>
    )
}