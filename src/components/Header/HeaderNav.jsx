import React from 'react'
import { Link } from 'react-router-dom'

import './HeaderNav.css'

export default props => {

    return (
        <nav className="header-nav">
            <ul>
                <li>
                    <Link to="/auline">Auline</Link>
                </li>
                <li>
                    <Link to="/add_friend">Adicionar Lobos</Link>
                </li>
                <li>
                    <Link to="/friends">Sua Alcateia</Link>
                </li>
            </ul>
        </nav>
    )
}