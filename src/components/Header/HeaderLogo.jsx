import React from 'react'

import './HeaderLogo.css'
export default props => {

    return (
        <img className="header-logo" src={require("../../assets/img/wolf.svg")} alt=""/>
    )
}