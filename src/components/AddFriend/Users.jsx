import React, {useState , useEffect} from 'react'
import axios from 'axios'

import User from './User'

import './Users.css'

export default props => {
    const [users, setUsers] = useState([])
    const [selfUser, setSelfUser] = useState([])
    
    useEffect( () => {
        axios.get(
                'https://wolfbook.herokuapp.com/validate_user', 
                { headers:{ "Content-Type": "application/json", "Authorization": localStorage.getItem('token') }}
            ).then (response => {
                console.log(response)
                setSelfUser(response.data.user)
            })
    }, [])

    useEffect( () => {
        axios.get(
                'https://wolfbook.herokuapp.com/users', 
                { headers:{ "Content-Type": "application/json", "Authorization": localStorage.getItem('token') }}
            ).then(response => {
                // console.log(response.data)
                setUsers(response.data)
            })
    }, [])
    
    return (
        <div className="users-list">
            {users.map( user => {
                if (user.email !== selfUser.email) {
                    return <User user={user} />
                }
            })}
        </div>
    )
}