import React from 'react'
import axios from 'axios'

import './User.css'

export default props => {

    const addFriend = () => {
        axios.post(
            'https://wolfbook.herokuapp.com/friendships/' + props.user.id, 
            { headers:{ "Content-Type": "application/json", "Authorization": localStorage.getItem('token') }},
        ).then(response => {
            console.log(response)
        })
    }

    return (
        <div className="user">
            <h2>{props.user.name}</h2>
            {/* <h2>{props.user.id}</h2> */}
            <button onClick={addFriend}>Adicionar amigo</button>
        </div>
    )
}