import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Friend from './Friend'

export default props => {
    const [friends, setFriends] = useState([])
    
    useEffect( () => {
        const axiosConfig = {
            headers:{
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem('token')
            }
        }

        axios.get('https://wolfbook.herokuapp.com/friendships', axiosConfig)
            .then(response => {
                console.log(response.data)
                // setFriends(response.data)
            })
    }, [])
    
    return (
        <div className="friends-list">
            {friends.map( friend => {
                return <Friend friend={friends} />
            })}
        </div>
    )
}