import React from 'react'

export default props => {

    return (
        <div className="friend-card">
            <h1>{props.friend.name}</h1>
        </div>
    )
}