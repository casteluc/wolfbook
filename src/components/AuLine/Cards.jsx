import React, { useState, useEffect } from 'react'
import axios from 'axios'

import Card from './Card'


export default props => {
    const [cards, setCards] = useState([])
    
    useEffect( () => {
        const axiosConfig = {
            headers:{
              "Content-Type": "application/json",
              "Authorization": localStorage.getItem('token')
            }
        }

        axios.get('https://wolfbook.herokuapp.com/posts', axiosConfig)
            .then(response => {
                // console.log(response)
                setCards(response["data"].reverse())
            })
    })

    return (
        <div className="cards-timeline">
            {cards.map( card => {
                return <Card 
                    cardName={card["user"]["name"]} 
                    cardMessage={card["content"]} 
                    comments={card["comments"]} 
                    cardId={card["id"]}
                />
            })}
        </div>
    )
}