import React, { useState, useEffect } from 'react'
import axios from 'axios'

import Comments from './Comments'
import CommentForm from './CommentForm'

import './Card.css'

export default props => {
    const [liked, setLiked] = useState(false)
    const [likes, setLikes] = useState(0)

    const axiosConfig = {
        headers:{
            "Content-Type": "application/json",
            "Authorization": localStorage.getItem('token')
        }
    }

    const handleLike = () => {
        setLiked(true)
        const postBody = {
            "like":{
                "likable_id": props.cardId,
                "likable_type": "Post"
            }
        }

        console.log(postBody)

        axios.post('https://wolfbook.herokuapp.com/likes', postBody, axiosConfig)
            .then(response => {
                console.log(response)
            })
    }

    useEffect( () => {
        axios.get('https://wolfbook.herokuapp.com/likes', axiosConfig)
            .then(response => {
                // console.log(response)
                // setLikes(response["data"])
            })
    })
    return (
        <div className="card">
            <div className="card-header">
                <h2>@{props.cardName}</h2>
                <button onClick={handleLike}>Like</button>
            </div>
            <p>{props.cardMessage}</p>
            
            <Comments cardId={ props.cardId } />
            <CommentForm cardId={ props.cardId } />
        </div>
    )
}