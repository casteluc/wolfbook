import React, { useEffect } from 'react'

import './Comment.css'
export default props => {
    return (
        <div className="card-comment">
            <h3>@{props.comment["user"]["name"]}:</h3>
            <p>{props.comment["content"]}</p>
        </div>
    )
}