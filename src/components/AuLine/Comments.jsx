import React, { useEffect, useState } from 'react'
import axios from 'axios'

import Comment from './Comment'

import "./Comments.css" 

export default props => {

    const [comments, setComments] = useState([])

    const axiosConfig = {
        headers:{
          "Content-Type": "application/json",
          "Authorization": localStorage.getItem('token')
        }
    }

    useEffect( () => {
        axios.get('https://wolfbook.herokuapp.com/comments', axiosConfig)
            .then(response => {
                // console.log(response)
                setComments(response["data"].reverse())
            })

    })

    return (
        <div className="card-comments">
            <h3>Comentários:</h3>
            {comments.map( comment => {
                if (comment["post"]["id"] === props.cardId) {
                    return <Comment comment={comment} />  
                }
            })}
        </div>
    )
}