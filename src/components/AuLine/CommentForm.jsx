import React, { useState } from 'react'
import axios from 'axios'

import './CommentForm.css'

export default props => {

    const [comment, setComment] = useState('')

    const handleInputChange = e => {
        setComment(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        
        const postBody = {
            "comment": {
                "content": comment,
                "post_id": props.cardId
            }
        }
        
        const axiosConfig = {
            headers:{
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem('token')
            }
        }
        
        axios.post('https://wolfbook.herokuapp.com/comments', postBody, axiosConfig)
            .then( response => {
                console.log("Comentário enviado")
            })
        
        setComment('')
    }

    return (
        <form onSubmit={handleSubmit} className="comment-input-form">

            <textarea cols="30" rows="2" placeholder="Escreva um comentário..." onChange={handleInputChange} value={comment}></textarea>

            <input type="submit" value="Postar"/>

        </form>
    )
}