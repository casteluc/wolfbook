import React, { useState } from 'react'
import axios from 'axios'

import './PostCardForm.css'

export default props => {

    const [message, setMessage] = useState('')

    const handleInputChange = e => {
        setMessage(e.target.value)
    }

    const handleSubmit = e => {
        e.preventDefault()
        
        const postBody = {
            "post": {
                "content": message
            }
        }
        
        const axiosConfig = {
            headers:{
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem('token')
            }
        }

        console.log(postBody)
        console.log(axiosConfig)
        
        axios.post('https://wolfbook.herokuapp.com/posts', postBody, axiosConfig)
        .then( response => {
            console.log("Mensagem enviada")
        })
        
        setMessage('')
    }

    return (
        <form onSubmit={handleSubmit} className="message-input-form">

            <textarea cols="30" rows="7" placeholder="No que você está pensando?" onChange={handleInputChange} value={message}></textarea>

            <input type="submit" value="Postar"/>

        </form>
    )
}