import React from 'react'

import Cards from '../components/AuLine/Cards'
import PostCardForm from '../components/AuLine/PostCardForm'

import './AuLine.css'

export default props => {

    return (
        <div className="auline">
            <h1>AuLine</h1>
            <PostCardForm />
            <Cards></Cards>
        </div>
    )
}