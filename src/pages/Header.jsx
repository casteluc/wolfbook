import React from 'react'

import HeaderLogo from '../components/Header/HeaderLogo'
import HeaderNav from '../components/Header/HeaderNav'
import LogoutButton from '../components/Header/LogoutButton'

import './Header.css'
export default props => {

    return (
        <header>
            <HeaderLogo />
            <HeaderNav />
            <LogoutButton />
        </header>
    )
}