import React from 'react'

import Users from '../components/AddFriend/Users'

export default props => {

    return (
        <div className="add-friend">
            <Users />
        </div>
    )
}