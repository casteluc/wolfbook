import React from 'react'

export default props => {

    return (
        <div className="not-found">
            <h1>Sorry, page not found</h1>
        </div>
    )
}