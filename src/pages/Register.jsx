import React from 'react'

import RegisterAbout from '../components/Register/RegisterAbout'
import RegisterForm from '../components/Register/RegisterForm'
import LoginDashboard from '../components/Register/Login/LoginDashboard'

import './Register.css'

// Página que exibe na tela uma breve descrição sobre o app, um formulário 
// de cadastros e um modal para logar no app
export default props => {

    return (
        <div className="register">
            <RegisterAbout title='Wolfbook'>A rede social feita para você se conectar com a sua alcateia!</RegisterAbout>

            <div className="register-interaction">
                <RegisterForm>Cadastre-se</RegisterForm>
                <LoginDashboard buttonText="Faça login">Já tem uma conta?</LoginDashboard>
            </div>
        </div>
    )
}