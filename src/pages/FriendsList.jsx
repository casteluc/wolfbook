import React from 'react'

import Friends from '../components/Friends/Friends'

export default props => {

    return (
        <div className="friends-list">
            <h1>Sua Alcatéia</h1>
            <Friends />
        </div>
    )
}