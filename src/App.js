import React, { useEffect, useState } from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom'

import history from './history'
import './App.css'
import Header from './pages/Header'
import Register from './pages/Register'
import AuLine from './pages/AuLine'
import Auth from './auth'
import { AuthProvider } from './Context/AuthContext';
import AddFriend from './pages/AddFriend';
import FriendsList from './pages/FriendsList';
import NotFound from './pages/NotFound'

function App() {
  const [isLogged, setIsLogged] = useState(false)

  const session = (bool) => {
    setIsLogged(bool)
  }

  useEffect( () => {
    Auth(session)
  }, [])

  return (
    <AuthProvider>
      <Router history={history}>

        {/* Retorna o header se o usuário não estiver na página de login */}
        {isLogged ? <Header /> : null}
        
        <Switch>

          <Route exact path="/"><Redirect to="/auline"/></Route>
          <Route exact path="/register">{isLogged ? <Redirect to="/auline"/> : <Register />}</Route>
          <Route exact path="/auline">{isLogged ? <AuLine/> : <Redirect to="/register"/>}</Route>
          <Route exact path="/add_friend">{isLogged ? <AddFriend/> : <Redirect to="/register"/>}</Route>
          <Route exact path="/friends">{isLogged ? <FriendsList/> : <Redirect to="/register"/>}</Route>
          <Route path="/"><NotFound /></Route>
        </Switch>

      </Router>
    </AuthProvider>
  );
}

export default App;
