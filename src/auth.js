import axios from 'axios'

export default function User(session) {

    const cacheToken = localStorage.getItem('token')

    const axiosConfig = {
        headers:{
            "Content-Type": "application/json",
            "Authorization": cacheToken
        }
    }

    axios.get('https://wolfbook.herokuapp.com/validate_user', axiosConfig)
        .then(response => {
            session(true)
        }).catch (error => {
            session(false)
        }) 
    
}

