/* eslint-disable no-unused-expressions */
import React, { createContext, useState } from 'react'
import axios from 'axios'

import history from '../history'
import { postBody } from '../components/Register/Login/LoginForm'

const Context = createContext()

function AuthProvider({children}) {
    // Função executada ao enviar o formulário de login
    async function handleLogin() {
        // Realiza a requisição HTTP e guarda o valor do token obtido na resposta na variável token
        await axios.post('https://wolfbook.herokuapp.com/login', postBody)
            .then(response => {
                console.log(response)
                localStorage.setItem('token', response["data"]["token"])
                window.location.reload()
            }).catch(error => {
                alert("Informações inválidas, não foi possível realizar o login")
            })
    }
    
    function handleLogout() {
        localStorage.removeItem('token')
        window.location.reload()
    }

    return (
        <Context.Provider value={{ handleLogin, handleLogout }}>
            {children}
        </Context.Provider>
    )
}

export { Context, AuthProvider }